﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using ProduktManager.Repository;
using ProduktManager.Model;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace ProduktManager.Views
{
    class MainWindowViewModel
    {
        private ProductContext _context =  new ProductContext();
          
        public MainWindowViewModel() {
            _context.Database.EnsureCreated();



            FillCategories();
            Fillprod();


        }

        public void Fillprod()
        {
           
            var queryResult = (from a in _context.Products
                               select a).ToList();
            this.Products = queryResult;
        }


        public void FillCategories() 
        {
            var queryResult = (from a in _context.Categories
                     select a).ToList();
            this.Categories = queryResult;
        }

     

        private List<Category> _categories;
        public List<Category> Categories 
        { 
            get 
            {
                return _categories;

            } 
            set
            {
                _categories = value;
                NotifyPropertyChanged();
            } 
        }

        private Category _selectedCategory;
        public Category SelectedCategory
        {
            get 
            {
                return _selectedCategory;
            }
            set 
            {
                _selectedCategory = value;
                NotifyPropertyChanged();
                FillProducts(value);
           
            }
                
        }


      

        private void FillProducts(Category cat)
        {
           
            Category category = this.SelectedCategory;
            List<Product> queryResult = (from p in _context.Products
                     where p.CategoryId == category.CategoryId
                     select p).ToList();
            

            this.Products = Categories.ElementAt(cat.CategoryId-1).Products; 
        }

        private List<Product> _products;
        public List<Product> Products
        {
            get
            {
                return _products;

            }
            set
            {
                _products = value;
                NotifyPropertyChanged();
            }
        }

        private Product _selectedProduct;
        public Product SelectedProduct
        {
            get
            {
                return _selectedProduct;

            }
            set
            {
                _selectedProduct = value;
                NotifyPropertyChanged();
            }
        }
        private ICommand _saveCommand = null;
        public ICommand SaveCommand
        {
            get
            {
                return _saveCommand = new RelayCommand(Save);
            }
        }

        private void Save()
        {
            _context.SaveChanges();
        }



        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


    }
}
