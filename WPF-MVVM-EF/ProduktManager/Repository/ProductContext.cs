﻿using Microsoft.EntityFrameworkCore;
using ProduktManager.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProduktManager.Repository
{
    public class ProductContext : DbContext
    {
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Category> Categories { get; set; }

        protected override void OnConfiguring(
            DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(
                "Data Source=products.db");
            optionsBuilder.UseLazyLoadingProxies();
            base.OnConfiguring(optionsBuilder);
        }
    }
    }
