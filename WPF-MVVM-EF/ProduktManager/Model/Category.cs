﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace ProduktManager.Model
{
    public class Category
    {
        public Category() {
            this.Products = new List<Product>();
        }
        public int CategoryId { get; set; }
        public string Name { get; set; }


        public virtual List<Product> Products { get; set; }



    }
}
