﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ProduktManager.Model
{
    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }

        public Nullable<int> CategoryId { get; set; }
        public virtual Category Category { get; set; }


       
    }
}
