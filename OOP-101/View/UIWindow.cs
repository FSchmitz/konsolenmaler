﻿using OOP_101.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_101
{

    class UIWindow

    {
       private bool running = true;

       private Painter painter;

        public UIWindow() {
            painter = new Painter();
            Welcome();
            Run();
        }
        public UIWindow(int whiteBoardID) {
            painter = new Painter(whiteBoardID);
            Welcome();
            Run();
        }

#nullable enable
        private void Run()
        {
            while (running)
            {
               char input = promptDrawing();
               IDrawable? drawing = TryDecideDrawable(input);
               if (drawing != null)
                    painter.Draw(drawing);
                painter.Show();               
            }
        }

        private IDrawable? TryDecideDrawable(char input) {
            try
            {
                return DecideDrawable(input);
            }
            catch(Exception e)
            {
                Console.WriteLine(String.Format("Fehler: {0}", e.Message));
                return null;
            }
        }

        private IDrawable? DecideDrawable(char input)
        {
            switch (input)
            {
                case 'd':
                case 'D':
                    return new Dreieck(2.2, "grün", 2);
                case 'k':
                case 'K':
                    return new Kreis(3.3, "blau", 5);
                case 'q':
                case 'Q':
                    return new Quadrat(2.2, "blau", 1);
                case 't':
                case 'T':
                    return new TextField(2.2, "Lorem Ipsum Sum", "Arial");
                case 'x':
                case 'X':
                    running = false;
                    return null;
                default:
                    throw new Exception("Eingabe unbekannt");

            }
        }

        private void Welcome()
        {
            Console.WriteLine("Willkommen im Konsolenmaler!\n" +
                "mit (x) beenden Sie das Program");
               
        }
        private char promptDrawing()
        {
            Console.WriteLine(
               "Was möchten Sie zeichnen?\n" +
               "(D)reieck, (K)reis, (Q)uadrat oder (T)ext?");
            return Console.ReadKey().KeyChar;
        }
    }
}
