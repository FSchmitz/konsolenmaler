﻿using OOP_101.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_101.Repository
{
    class DreieckRepository
    {
        private DreieckDB dreieckDB;
        public DreieckRepository() {
            dreieckDB = new DreieckDB();
        }

        public Dreieck GetDreick(int whiteBoardID) {
            string startpunkt;
            string farbe;
            string schenkellänge;
            
            bool tryStartpunkt = dreieckDB.Dreick.TryGetValue("startpunkt", out startpunkt );
            bool tryFarbe = dreieckDB.Dreick.TryGetValue("farbe", out farbe);
            bool trySchenkellänge = dreieckDB.Dreick.TryGetValue("schenkellänge", out schenkellänge);

            if (tryStartpunkt && tryFarbe && trySchenkellänge) {
                return new Dreieck(Double.Parse(startpunkt), farbe, int.Parse(schenkellänge));
            }
            else
            {
                throw new Exception("konnte Kein Dreieck von der DB lesen");
            }

            
        }
    }
}
