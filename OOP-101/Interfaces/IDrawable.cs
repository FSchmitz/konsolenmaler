﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_101
{
    interface IDrawable
    {
        public string Draw();
    }
}
