﻿using OOP_101.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace OOP_101
{
    class Painter
    {
        private DreieckRepository dreieckRepository;

        private Whiteboard whiteboard;
       

        public Painter() {
            dreieckRepository = new DreieckRepository();
            whiteboard = new Whiteboard();
        }

        public Painter(int whiteBoardID)
        {
            dreieckRepository = new DreieckRepository();
            LoadDBWhiteboard(whiteBoardID);
        }

        public void LoadDBWhiteboard(int whiteBoardID)
        {
            whiteboard = new Whiteboard();
            whiteboard.AddDrawing(dreieckRepository.GetDreick(whiteBoardID));
        }

       

        public void Draw(IDrawable drawing)
        {
            int depth = CalculateDepth();
            whiteboard.AddDrawing(drawing, depth);
        }
        private int CalculateDepth() {
            if (whiteboard.Content.Count == 0) return 0;
            int depth = whiteboard.Content.ElementAt(whiteboard.Content.Count-1).Depth;
            int depthcount = whiteboard.Content.Select((x) => x.Depth == depth).Count();
            if (depthcount < 3)
                return depthcount;
            else
                return depthcount++;
        }
        public void Show()
        {
            Console.WriteLine(Environment.NewLine);
            whiteboard.Content.ForEach((x) => Console.WriteLine(x.Draw()));
        }
    }
}
