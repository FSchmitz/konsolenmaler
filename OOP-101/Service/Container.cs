﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_101
{
    class Container<T> : IDrawable where T : IDrawable
    {
        internal int Depth { get; set; }

        internal T Payload { get; set; }

        public Container(T payload, int depth = 1)
        {
            Payload = payload;
            Depth = depth;
        }

        public string Draw()
        {
            return Payload.Draw() + " auf der Ebene: " + Depth;
        }
    }
}
