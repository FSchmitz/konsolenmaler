﻿using OOP_101.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace OOP_101
{
    class Whiteboard
    {

        internal List<Container<IDrawable>> Content { get; set; }

        public Whiteboard()
        {
            Content = new List<Container<IDrawable>>();        
        }

        public void AddDrawing(IDrawable drawing, int depth=1)
        {
            Content.Add(new Container<IDrawable>(drawing,depth));
        }

        public void Show()
        {
            Content.ForEach((x) => Console.WriteLine(x.Draw()));
          
        }


        

        
        

    }
}
