﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_101
{
    class Dreieck : Form, IDrawable
    {
        private int sidelength;
        public Dreieck(double point, string color, int sidelength) : base(point, color)
        {
            this.sidelength = sidelength;
        }

        public void ChangeSidelength(int sidelength)
        {
            if (sidelength > 0)
                this.sidelength = sidelength;
        }

        public string Draw()
        {
            return String.Format("Ein Dreieck auf: {0} mit der Farbe: {1} und der Seitenlänge {2}", point, color, sidelength);
        }
    }
}
