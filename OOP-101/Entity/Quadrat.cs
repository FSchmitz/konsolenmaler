﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_101
{
    class Quadrat : Form, IDrawable
    {

        private int edgelength;

        public Quadrat(double point, string color, int edgelength) : base(point, color)
        {
            ChangeEdgelengthlength(edgelength);
        }

        public void ChangeEdgelengthlength(int edgelength)
        {
            if (edgelength > 0)
                this.edgelength = edgelength;
        }

        public string Draw()
        {
            return String.Format("Ein Quadrat auf: {0} mit der Farbe: {1} und der Kantenlänge {2}", point, color, edgelength);
        }
    }
}
