﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_101
{
    class Form
    {
        protected double point;
        protected string color;

        public Form(double point, string color)
        {
            this.color = color;
            this.point = point;
        }

        public void ChangeColor(string color)
        {
            this.color = color;
        }

    }
}
