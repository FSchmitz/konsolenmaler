﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_101
{
    class Kreis : Form, IDrawable
    {

        private int radius = 1;

        public Kreis(double point, string color, int radius) : base(point, color)
        {
            ChangeRadius(radius);
        }

        public void ChangeRadius(int radius)
        {
            if (radius > 0)
                this.radius = radius;
        }

        public string Draw()
        {
            return String.Format("Ein Kreis auf: {0} mit der Farbe: {1} und dem Radius: {2}", point, color, radius);
        }

    }
}
