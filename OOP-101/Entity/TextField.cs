﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_101.Entity
{
    class TextField : IDrawable
    {
        private double point;
        private string content;
        private string font;

        public TextField(double point, string content, string font)
        {
            this.point = point;
            this.content = content;
            this.font = font;
        }
        public string Draw()
        {
            return String.Format("Ein Textfeld auf: {0} mit der Schriftart: {1} und dem Inhalt: {2}", point, content, font);
        }

    }
}
