﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace OOP_101.Database
{
    public class DreieckDB
    {
        public Dictionary<string, string> Dreick;

        public DreieckDB() {
            initDB();
        }

        private void initDB()
        {
            Dreick = new Dictionary<string, string>();
            Dreick.Add("startpunkt", "7");
            Dreick.Add("farbe", "schwarz");
            Dreick.Add("schenkellänge", "5");
        }
       
    }
}